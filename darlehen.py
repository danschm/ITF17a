welcome_message = '***********************************************************\n' \
                  '**                                                       **\n' \
                  '**   Willkommen zum Darlehensrechner                     **\n' \
                  '**   Version: 0.2 vom 05.10.2017                         **\n' \
                  '**   Fehler bitte an: daniel.schmiing@gmail.com          **\n' \
                  '**                                                       **\n' \
                  '***********************************************************'

print(welcome_message)

from decimal import *

def endfälligesDarlehen():
    # Eingabe
    darlehen = Decimal(input("\nBitte Darlehen in € eingeben: "))
    zinssatz = Decimal(input("Bitte Zinssatz in % eingeben: "))/100
    laufzeit = Decimal(input("Bitte Laufzeit in Jahren eingeben: "))

    # Verarbeitung
    zinsen = darlehen * zinssatz
    zinsen_gesamt = zinsen * laufzeit
    kosten_gesamt = darlehen + zinsen_gesamt

    # Ausgabe
    print("\n{0:15,.2f}€ Zinsen".format(zinsen))
    print("{0:15,.2f}€ Summe Zinsen über {1:1f} Jahr(e) Laufzeit".format(zinsen_gesamt, laufzeit))
    print("{0:15,.2f}€ Summe Rückzahlung zum Ende der Laufzeit".format(kosten_gesamt))

    another = input("\nWeiteres endfälliges Darlehen berechnen? j/n ")
    if another == "j":
        endfälligesDarlehen()


endfälligesDarlehen()

